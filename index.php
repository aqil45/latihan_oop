<?php

    require_once ("animal.php");
    require_once ("frog.php");
    require_once ("kerasakti.php");

    $animal = new Animal("Shaun");
    echo "Name Animal :".  $animal -> name . "<br>";
    echo "Total Legs :". $animal -> legs . "<br>";
    echo "Cold Blooded :". $animal -> cold_blooded . "<br><br>";
    
    $kodok = new Frog ("Buduk");
    echo "Name Animal :". $kodok -> name . "<br>";
    echo "Total Legs :". $kodok -> legs . "<br>";
    echo "Cold Blooded :". $kodok -> cold_blooded . "<br>";
    echo $kodok -> Jump(); 
    echo "<br><br>";

    $kera = new Sungokong ("Kera sakti"); 
    echo "Name Animal :". $kera -> name . "<br>";
    echo "Total Legs :". $kera -> legs . "<br>";
    echo "Cold Blooded :". $kera -> cold_blooded . "<br>";
    echo $kera -> Yell(); 

?>